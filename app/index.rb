require 'rubygems'
require 'sinatra'
require 'data_mapper'
require 'digest/md5'
require 'fileutils'
require 'yaml'
require 'date'

enable :inline_templates

DataMapper.setup(:default, "sqlite3://#{Dir.pwd}/database.db")

class Upload
  include DataMapper::Resource
  property :id, Serial
  property :identifier, String
  property :filename, String
  property :type, String
  property :extension, String
  property :created_at, String
end

DataMapper.finalize
DataMapper.auto_upgrade!

helpers do
  def protected!
    unless authorized?
      response['WWW-Authenticate'] = %(Basic realm="Restricted Area")
      throw(:halt, [401, "Not authorized\n"])
    end
  end

  def authorized?
    @auth ||=  Rack::Auth::Basic::Request.new(request.env)
    config ||= YAML.load_file('app/config.yml')
    @auth.provided? && @auth.basic? && @auth.credentials && @auth.credentials == [config['auth']['username'], config['auth']['password']]
  end

  def not_found
    "404"
  end
end

get '/' do
  protected!
  erb :form
end

post '/' do
  filename = Digest::MD5.hexdigest("#{rand()} secr3t #{params[:file][:filename]}")
  identifier = filename[0..5]
  extension = params[:file][:filename].split(".").last
  if Upload.create(identifier: identifier, filename: filename, extension: extension, type: params[:file][:type])
    FileUtils.cp(params[:file][:tempfile], "files/#{filename}.#{extension}")
    "your link is <a href=\"#{request.url}#{identifier}\">#{request.url}#{identifier}</a>"
  end
end

get '/:identifier' do
  file = Upload.last(:identifier => params[:identifier])
  if file
    filename = "files/#{file.filename}.#{file.extension}"
    if File::exists?(filename) && DateTime.parse(file.created_at) > (DateTime.now - 2)
      send_file(filename, :disposition => 'inline', :type => file.type)
    else
      "t-#{not_found}"
    end
  else
    not_found
  end
end

__END__

@@ form
<form action="" method="post" enctype="multipart/form-data">
<p>
<label for="file">File:</label>
<input type="file" name="file">
</p>

<p>
<input name="commit" type="submit" value="Upload" />
</p>
</form>